package co.com.choucair.certification.projectChallenge.stepdefinitions;

import co.com.choucair.certification.projectChallenge.model.uTestData;
import co.com.choucair.certification.projectChallenge.questions.Answer;
import co.com.choucair.certification.projectChallenge.tasks.OpenUp;
import co.com.choucair.certification.projectChallenge.tasks.Register;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

public class uTestStepDefinitions {
    @Before
    public void setStage() {
        OnStage.setTheStage(new OnlineCast());
    }
    @Given("^Joseph wants to register in the uTest page$")
    public void josephWantsToRegisterInTheUTestPage() {
        OnStage.theActorCalled("Joseph").wasAbleTo(OpenUp.thePage());
    }
    @When("^he fills of form for register$")
    public void heFillsOfFormForRegister(List<uTestData> uTestData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(Register.fillForm(uTestData.get(0).getTxtFirstName(), uTestData.get(0).getTxtLastname(), uTestData.get(0).getTxtEmail(), uTestData.get(0).getTxtBirthMonth(), uTestData.get(0).getTxtBirthDay(), uTestData.get(0).getTxtBirthYear(), uTestData.get(0).getTxtPostalCode(), uTestData.get(0).getTxtPassword()));
    }
    @Then("^he is user of the uTest page$")
    public void heIsUserOfTheUTestPage(List<uTestData> uTestData) throws Exception {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.tothe(uTestData.get(0).getTxtTitle())));
    }
}
