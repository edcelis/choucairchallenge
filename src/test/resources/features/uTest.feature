#Autor: Elian Celis
@stories
Feature: uTest
  A user want to tested new technologies for the test of the software with uTest
  @scenario1
  Scenario: Register in the uTest page
  Given Joseph wants to register in the uTest page
  When he fills of form for register
    | txtFirstName | txtLastname | txtEmail | txtBirthMonth | txtBirthDay | txtBirthYear | txtPostalCode | txtPassword |
    | Joseph | Chavez | Jchavez@utest.com | September | 19 | 1998 | 760001 | ChoucairChallenge2021+ |
  Then he is user of the uTest page
    | txtTitle |
    | The last step |
