package co.com.choucair.certification.projectChallenge.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class uTestRegisterPage {
    public static final Target BUTTON_REDIRECT = Target.the("button to redirect to form register").located(By.xpath("//a[@href='/signup/personal']"));
    public static final Target TXT_FIRST_NAME = Target.the("text box to type the first name").located(By.id("firstName"));
    public static final Target TXT_LASTNAME = Target.the("text box to type the lastname").located(By.id("lastName"));
    public static final Target TXT_EMAIL = Target.the("text box to type the email address").located(By.id("email"));
    public static final Target SELECT_BIRTH_MONTH = Target.the("list to select the birth month").located(By.id("birthMonth"));
    public static final Target SELECT_BIRTH_DAY = Target.the("list to select the birth day").located(By.id("birthDay"));
    public static final Target SELECT_BIRTH_YEAR = Target.the("list to select the birth year").located(By.id("birthYear"));
    public static final Target BUTTON_NEXT_SECTION_1 = Target.the("button to redirect to next section (address) of form register").located(By.xpath("//a[@class='btn btn-blue']"));
    public static final Target TXT_CITY = Target.the("text box to type the city").located(By.id("city"));
    public static final Target TXT_POSTAL_CODE = Target.the("text box to type the postal code").located(By.id("zip"));
    public static final Target BUTTON_NEXT_SECTION_2 = Target.the("button to redirect to next section (devices) of form register").located(By.xpath("//a[@class='btn btn-blue pull-right']"));
    public static final Target BUTTON_NEXT_SECTION_3 = Target.the("button to redirect to next section (last step) of form register").located(By.xpath("//a[@class='btn btn-blue pull-right']"));
    public static final Target TXT_PASSWORD = Target.the("text box to type the password").located(By.id("password"));
    public static final Target TXT_CONFIRM_PASSWORD = Target.the("text box to type the confirm password").located(By.id("confirmPassword"));
    public static final Target CHECK_STAY_INFORMED = Target.the("check box to the verify stay informed").located(By.name("newsletterOptIn"));
    public static final Target CHECK_TERMS = Target.the("check box to the verify terms of use").located(By.id("termOfUse"));
    public static final Target CHECK_PRIVACY= Target.the("check box to the verify privacy and security policy").located(By.id("privacySetting"));
    public static final Target BUTTON_COMPLETE_REGISTER = Target.the("button to complete and send the form register").located(By.xpath("//a[@class='btn btn-blue']"));
    public static final Target SUBTITLE = Target.the("subtitle from form register").located(By.xpath("//span[@class='sub-title']"));

}
