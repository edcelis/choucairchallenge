package co.com.choucair.certification.projectChallenge.questions;

import co.com.choucair.certification.projectChallenge.userinterface.uTestRegisterPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Answer implements Question<Boolean> {
    private String question;

    public Answer(String question) {
        this.question = question;
    }
    public static Answer tothe(String question) {
        return new Answer(question);
    }
    @Override
    public Boolean answeredBy(Actor actor) {
        boolean result;
        String nameCourse = Text.of(uTestRegisterPage.SUBTITLE).viewedBy(actor).asString();
        if(question.equals(nameCourse)) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }
}