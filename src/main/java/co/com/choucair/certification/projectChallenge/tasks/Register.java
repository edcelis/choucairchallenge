package co.com.choucair.certification.projectChallenge.tasks;

import co.com.choucair.certification.projectChallenge.userinterface.uTestRegisterPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Register implements Task {
    private String txtFirstName;
    private String txtLastname;
    private String txtEmail;
    private String txtBirthMonth;
    private String txtBirthDay;
    private String txtBirthYear;
    private String txtPostalCode;
    private String txtPassword;

    public Register(String txtFirstName, String txtLastname, String txtEmail, String txtBirthMonth, String txtBirthDay, String txtBirthYear, String txtPostalCode, String txtPassword) {
        this.txtFirstName = txtFirstName;
        this.txtLastname = txtLastname;
        this.txtEmail = txtEmail;
        this.txtBirthMonth = txtBirthMonth;
        this.txtBirthDay = txtBirthDay;
        this.txtBirthYear = txtBirthYear;
        this.txtPostalCode = txtPostalCode;
        this.txtPassword = txtPassword;
    }

    public static Register fillForm(String txtFirstName, String txtLastname, String txtEmail, String txtBirthMonth, String txtBirthDay, String txtBirthYear, String txtPostalCode, String txtPassword) {
        return Tasks.instrumented(Register.class, txtFirstName, txtLastname, txtEmail, txtBirthMonth, txtBirthDay, txtBirthYear, txtPostalCode, txtPassword);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(uTestRegisterPage.BUTTON_REDIRECT),
                Enter.theValue(txtFirstName).into(uTestRegisterPage.TXT_FIRST_NAME),
                Enter.theValue(txtLastname).into(uTestRegisterPage.TXT_LASTNAME),
                Enter.theValue(txtEmail).into(uTestRegisterPage.TXT_EMAIL),
                SelectFromOptions.byVisibleText(txtBirthMonth).from(uTestRegisterPage.SELECT_BIRTH_MONTH),
                SelectFromOptions.byVisibleText(txtBirthDay).from(uTestRegisterPage.SELECT_BIRTH_DAY),
                SelectFromOptions.byVisibleText(txtBirthYear).from(uTestRegisterPage.SELECT_BIRTH_YEAR),
                Click.on(uTestRegisterPage.BUTTON_NEXT_SECTION_1),
                //Enter.theValue("Cali").into(uTestRegisterPage.TXT_CITY),
                Enter.theValue(txtPostalCode).into(uTestRegisterPage.TXT_POSTAL_CODE),
                Click.on(uTestRegisterPage.BUTTON_NEXT_SECTION_2),
                Click.on(uTestRegisterPage.BUTTON_NEXT_SECTION_3),
                Enter.theValue(txtPassword).into(uTestRegisterPage.TXT_PASSWORD),
                Enter.theValue(txtPassword).into(uTestRegisterPage.TXT_CONFIRM_PASSWORD),
                Click.on(uTestRegisterPage.CHECK_STAY_INFORMED),
                Click.on(uTestRegisterPage.CHECK_TERMS),
                Click.on(uTestRegisterPage.CHECK_PRIVACY),
                Click.on(uTestRegisterPage.BUTTON_COMPLETE_REGISTER)
        );
    }
}
