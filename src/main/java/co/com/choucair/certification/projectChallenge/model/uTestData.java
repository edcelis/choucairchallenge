package co.com.choucair.certification.projectChallenge.model;

public class uTestData {
    private String txtFirstName;
    private String txtLastname;
    private String txtEmail;
    private String txtBirthMonth;
    private String txtBirthDay;
    private String txtBirthYear;
    private String txtPostalCode;
    private String txtPassword;
    private String txtTitle;

    public String getTxtFirstName() {
        return txtFirstName;
    }
    public void setTxtFirstName(String txtFirstName) {
        this.txtFirstName = txtFirstName;
    }
    public String getTxtLastname() {
        return txtLastname;
    }
    public void setTxtLastname(String txtLastname) {
        this.txtLastname = txtLastname;
    }
    public String getTxtEmail() {
        return txtEmail;
    }
    public void setTxtEmail(String txtEmail) {
        this.txtEmail = txtEmail;
    }
    public String getTxtBirthMonth() {
        return txtBirthMonth;
    }
    public void setTxtBirthMonth(String txtBirthMonth) {
        this.txtBirthMonth = txtBirthMonth;
    }
    public String getTxtBirthDay() {
        return txtBirthDay;
    }
    public void setTxtBirthDay(String txtBirthDay) {
        this.txtBirthDay = txtBirthDay;
    }
    public String getTxtBirthYear() {
        return txtBirthYear;
    }
    public void setTxtBirthYear(String txtBirthYear) {
        this.txtBirthYear = txtBirthYear;
    }
    public String getTxtPostalCode() {
        return txtPostalCode;
    }
    public void setTxtPostalCode(String txtPostalCode) {
        this.txtPostalCode = txtPostalCode;
    }
    public String getTxtPassword() {
        return txtPassword;
    }
    public void setTxtPassword(String txtPassword) {
        this.txtPassword = txtPassword;
    }

    public String getTxtTitle() {
        return txtTitle;
    }

    public void setTxtTitle(String txtTitle) {
        this.txtTitle = txtTitle;
    }
}
